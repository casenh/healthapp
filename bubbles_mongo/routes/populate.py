#!/usr/bin/env python

import subprocess

subprocess.call(['mongoimport', '--db', 'medication', '--collection', 'information', '--type', 'json', '--file', './JSON/medication.json'])
