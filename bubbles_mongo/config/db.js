var User = require('./user');
var Encounter = require('./encounter');
var Prescription = require('./prescription');

/* Get information for a specific user */
this.get_user_info = function(username, callback) {

	User.get_info(username, function(userInfo) {
		callback(userInfo);
	});
};
