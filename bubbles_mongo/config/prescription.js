// config/passport.js

/* Load the user model */
var mongoose = require('mongoose');
var prescriptionSchema = require("../models/prescription");
//var db = mongoose.createConnection('mongodb://localhost/prescriptions');
var db = require("../models/dbConnection")
var Prescription = db.Prescriptions.model('Prescription', prescriptionSchema);
var Patient = require("../config/patient");

/* Grab each type of encounter that we have */
//var db = mongoose.createConnection('mongodb://localhost/prescriptions');



this.add = function(req, res, patientID, done) {

	var newPrescription = new Prescription();

	newPrescription.strength = req.body.strength;
	newPrescription.patientID = patientID;
	newPrescription.doctorID= req.session.id;
	newPrescription.name = req.body.name;
	newPrescription.quantity = req.body.quantity;
	newPrescription.dosage = req.body.dosage;
	newPrescription.frequency = req.body.frequency;
	newPrescription.refills = req.body.refills;
	newPrescription.start_date = req.body.start_date;
	newPrescription.end_date = req.body.end_date;
	newPrescription.pharmacy = req.body.pharmacy;
	
	newPrescription.dispense_inOffice = "No";
	if(req.body.dispense_inOffice)
		newPrescription.dispense_inOffice = "Yes";
	newPrescription.brand_necessary = "No";
	if(req.body.brand_necessary)
		newPrescription.brand_necessary = "Yes";
	newPrescription.rx_outside = "No";
	if(req.body.brand_necessary)
		newPrescription.rx_outside = "Yes";
	newPrescription.otc = "No";
	if(req.body.otc)
		newPrescription.otc = "Yes";
	newPrescription.print_deaNum = "No";
	if(req.body.print_deaNum)
		newPrescription.print_deaNum = "Yes";

	newPrescription.save(function(err) {
		if(err)
			throw err;
		else {

			Patient.addPrescription(req, res, patientID, newPrescription._id, function(req, res) {
				done(req, res);
			});
		}
	});
};


this.renewPrescription = function(req, res, done) {

	Encounter.find({username: req.session.username}, function(err, encounters) {
		var encounterList = [];
		for(var i = 0; i < encounters.length; i++) {
			for(var n = 0; n < encounters[i].tb_assessment.length; n++) {
				encounterList.push(encounters[i].tb_assessment[n]);
			}
		}
		done(req, res, encounterList);
	});
}

this.getPrescriptions = function(req, res, patientList, done) {
	
	var prescriptionID = [];
	for(var i = 0; i < patientList.length; i++) {
		for(var n = 0; n < patientList[i].prescription_id.length; n++) {
			prescriptionID.push(patientList[i].prescription_id[n]);
		}
	}

	Prescription.find({_id: {$in: prescriptionID}}, function(err, prescriptionList) {
		if(err)
			throw err;
		else {
			done(req, res, prescriptionList);
		}
	});
};
