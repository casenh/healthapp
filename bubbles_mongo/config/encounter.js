// config/passport.js

/* Load the user model */
var mongoose = require('mongoose');
var encounterSchema = require("../models/encounter");
var oralCancerSchema = require("../models/oral_cancer_assessment");
var tbAssessmentSchema = require("../models/tb_assessment");
//var db = mongoose.createConnection('mongodb://localhost/encounters');
var db = require("../models/dbConnection")

/* Create each of the counters that we will be adding to the database */
var Encounter = db.Encounter.model('Encounter', encounterSchema);
var oralCancerEncounter = db.Encounter.model('oralCancerEncounter', oralCancerSchema);
var tbAssessmentEncounter = db.Encounter.model('TB_Assessment', tbAssessmentSchema);


this.init = function(req, res, done) {

	var newEncounter = new Encounter();
	newEncounter.username = req.session.username;
	newEncounter.patientID = res.session.patientID;
	
	newEncounter.save(function(err) {
		if(err)
			throw err;
		else {

			newTbAssessment = new tbAssessmentEncounter();
			newTbAssessment.parentID = newEncounter._id;
			newTbAssessment.save(function(err) {
				if(err)
					throw err;
				else {

					done(req, res);
				}
			});
		}
	});
};

this.test = function(req, res, parentIDs, IDs, doctorID, done) {

	Encounter.find({}, function(err, encounterLists) {
		encounterLists.forEach(function(encounter) {
			encounter.id('1');
			for(property in encounter) {
				
			}
		});
	});
};


this.addDoctor = function(req, res, parentIDs, IDs, doctorID, done) {

	Encounter.find({_id: {$in: parentIDs}}, function(err, encounterList) {
		for(var i = 0; i < encounterList.length; i++) {
			if(encounterList[i].doctorID.indexOf(doctorID) == -1)
				encounterList[i].doctorID.push(doctorID);
			for(var n = 0; n < IDs.length; n++) {
				var tbDoc = encounterList[i].tb_assessment.id(IDs[n]);
				if(tbDoc && tbDoc.doctorID.indexOf(doctorID) == -1) {
					tbDoc.doctorID.push(doctorID);
				}
			}
		}

		var count = 0;
		encounterList.forEach(function(encounter) {
			encounter.save(function(err) {
				if(err)
					throw err;
				count++;
				if(count == encounterList.length) {
					done(req, res);
				}
			});
		});
	});
};


/*  Create a tb Assessment */
this.init_tb_assessment = function(req, res, done) {

	Encounter.findOne({patientID: req.body.patientID}, function(err, encounterHistory) {

		var newTBAssessment = new tbAssessmentEncounter();
		newTBAssessment.create(req, res, encounterHistory._id, function(req, res) {
			encounterHistory.tb_assessment.push(newTBAssessment);

			/* See if this doctor already has access to the patient's encounters */
			if(encounterHistory.doctorID.indexOf(req.body.doctorID) == -1) {
				if(req.session.userType == "Doctor")
					encounterHistory.doctorID.push(req.session.doctorID);
				//else
				//	encounterHistory.doctorID.push(req.body.doctorID);
			}

			encounterHistory.save(function(err) {
				if(err)
				throw err;
			done(req, res, encounterHistory);
			});
		});
	});
};

this.add_tb_assessment = function(req, res, patientID, encounterID, done) {

	Encounter.findOne({patientID: patientID}, function(err, encounterList) {
		var encounterInfo = encounterList.tb_assessment.id(encounterID);
		encounterInfo.update(req, res, function(req, res) {
			encounterList.save(function(err) {
				if(err)
					throw err;
				else
					done(req, res);
			});
		});
	});
};

this.get_tb_assessment = function(req, res, patientID, encounterID, done) {

	Encounter.findOne({patientID: patientID}, function(err, encounterList) {

			var encounterInfo = encounterList.tb_assessment.id(encounterID);
			done(req, res, encounterInfo);
	});
};

/* Find and return all encounters that are tagged with the current user's username */
this.find = function(req, res, done) {

	/* Find all the patients with that name */
	Encounter.find({username: req.session.username},  function(err, encounter) {
		if(err)
			res.render('encounter');
		if(encounter.length > 0) {
			done(req, res, {encounterList: encounter, user: req.session.username});
			//return {encounterList: encounter, user: req.session.username};
			//res.render('encounter', {encounterList: encounter, user: req.session.username});
		}
		else {
			done(req, res, {user: req.session.username});
		}
	});

};

/* Store the information from the oral cancer questionnaire */
this.add_oral_cancer = function(req, res, done) {

	/* Create the model and pull all the data from the webpage */
	var encounter = new oralCancerEncounter();

	encounter.username = req.session.username;
	encounter.patientID = req.body.patientID;
	encounter.familyHistory = req.body.family_history.yes;
	encounter.educationLevel = req.body.educationLevel;
	encounter.employed = req.body.employed;
	encounter.drinker = req.body.drinker;
	encounter.smoker = req.body.smoker;
	encounter.tobacco = req.body.tobacco;
	encounter.dentist = req.body.dentist;
	encounter.brush_teeth = req.body.toothbrush;
	encounter.toothpowder = req.body.toothpowder;
	encounter.lesion = req.body.lesion;
	encounter.hospital = req.body.hospital;
	encounter.high_risk = req.body.risk;

	encounter.save(function(err) {
		if(err) {
			throw err;
		}
		else {
			Encounter.find({username: req.session.username}, function(err, encounter) {
				if(err)
					res.render('encounter', {user: req.session.username});
				if(encounter.length > 0) {
					done(req, res, {encounterList: encounter, user: req.session.username});
				}
				else {
					done(req, res, {user: req.session.username});
				}
			});
		}
	});

};

/*this.add_tb_assessment = function (req, res, done) {

	var encounter = new tbAssessmentEncounter();

	encounter.username = req.session.username;
	encounter.patientID = req.body.patientID;
	if(req.body.cough != undefined)
		encounter.cough = true;
	else
		encounter.cough = false;
	if(req.body.fever != undefined)
		encounter.fever = true;
	else
		encounter.fever = false;
	if(req.body.poor_weight_gain != undefined)
		encounter.poor_weight_gain = true;
	else
		encounter.poor_weight_gain = false;
	if(req.body.night_sweats != undefined)
		encounter.night_sweats = true;
	else
		encounter.night_sweats = false;
	if(req.body.fatigue != undefined)
		encounter.night_sweats = true;
	else
		encounter.night_sweats = false;
	if(req.body.chest_pain != undefined)
		encounter.chest_pain = true;
	else
		encounter.chest_pain = false;
	encounter.contact = req.body.contact;
	encounter.afb_results = req.body.afb_results;
	encounter.anti_biotics = req.body.anti_biotics;

	encounter.save(function(err) {
		if(err) {
			throw err;
		}
		else {
			Encounter.find({username: req.session.username}, function(err, encounter) {
				if(err)
					done(req, res, {user: req.session.username});
				if(encounter.length > 0) {
					done(req, res, {encounterList: encounter, user: req.session.username});
				}
				else {
					done(req, res, {user: req.session.username});
				}
			});
		}
	});

};*/
