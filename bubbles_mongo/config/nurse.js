// config/passport.js

/* Load the user model */
var mongoose = require('mongoose');
var nurseSchema = require("../models/nurse");
var doctorSchema = require("../models/doctor");
//var db = mongoose.createConnection('mongodb://localhost/entities');
var db = require("../models/dbConnection")
var Nurse = db.Entities.model('Nurse', nurseSchema);
var Doctor = db.Entities.model('doctor', doctorSchema);

/* Grab each type of encounter that we have */
//var db_encounter = mongoose.createConnection('mongodb://localhost/encounters');
var encounterSchema = require('../models/encounter');
var Encounter = db.Encounter.model('Encounter', encounterSchema);
var tbAssessmentSchema = require('../models/tb_assessment');
var tbEncounter = db.Encounter.model('TB_Assessment', tbAssessmentSchema);


this.getEncounters = function(req, res, done) {
	/* TODO: can use req.session.nurseID and skip the first findOne */
	Nurse.findOne({nurseID: req.session.nurseID}, function(err, nurseInfo) {
		Encounter.find({doctorID: nurseInfo.doctorList_ID[0]}, function(err, encounterListAll) {
			var encounterList = [];
			for(var i = 0; i < encounterListAll.length; i++) {
				for(var n = 0; n < encounterListAll[i].tb_assessment.length; n++) {
					encounterList.push(encounterListAll[i].tb_assessment[n]);
				}
			}
			done(req, res, encounterList);
		});
	});
}

/* Add a nurse to the data base. Checks to make sure that the nurse doesn't already exist */
this.add = function(req, res, done) {
	
	/* Getting all the required information about the nurse */
	username = req.session.username;
	nurseID = req.body.nurseID;
	nurseName = req.body.nurseName;
	nurseAddress = req.body.nurseAddress;
	nurseState = req.body.nurseState;
	nurseCity = req.body.nurseCity;
	nurseZip = req.body.nurseZip;
	nurseHospital = req.body.nurseHospital;
	nurseBirthdate = req.body.nurseBirthdate;
	nurseGender = req.body.nurseGender;
	nurseEmail = req.body.nurseEmail;

	/* Check to see if they already have a nurse under this user */
	Nurse.findOne({'nurseID': nurseID}, function(err, user) {
		/* If the medical ID # has already been registered, a mistake has been made */	
		if(err) {
			done(req, res, false);
		}
		else {
			/* If the nurse is new to the user, add it */
			var newNurse = new Nurse();
			newNurse.username = req.session.username;
			newNurse.nurseID = nurseID;
			newNurse.nurseName = nurseName;
			newNurse.nurseAddress = nurseAddress;
			newNurse.nurseState = nurseState;
			newNurse.nurseCity = nurseCity;
			newNurse.nurseZip = nurseZip;
			newNurse.nurseHospital = nurseHospital;
			newNurse.nurseBirthdate = nurseBirthdate;
			newNurse.nurseGender = nurseGender;
			newNurse.nurseEmail = nurseEmail;
			newNurse.doctorList_objectID = [];
			newNurse.doctorList_ID = [];
			newNurse.doctorList_name = [];
			
			newNurse.save(function(err) {
				if(err) {
					done(req, res, false);
				}
				else {
					done(req, res, true);
				}
			});
		}
	});
};

/* TODO: Need to make one method for nurse's/nurse's and one for nurses */
this.find = function(req, res, done) {

	username = req.session.username;

	/* Find all the nurses with that name */
	Nurse.findOne({'username': username}, function(err, nurseInfo) {

		done(req, res, nurseInfo);
	});

};


this.find_byID = function(req, res, nurseID, done) {

	/* Grab the information about that nurse */
	Nurse.find({'nurseID': nurseID}, function(err, nurseInfo) {
		done(req, res, nurseInfo);
	});
}

this.get_doctor_list = function (req, res, done) {

	Nurse.find({}, function(err, nurseList) {
		done(req, res, nurseList);
	});
}

this.getInfo = function(req, res, done) {

	Nurse.findOne({username: req.session.username}, function(err, nurseInfo) {

		done(req, res, nurseInfo);
	});
}

this.addPatient = function(req, res, patientID, nurseID, done) {

	Nurse.findOne({nurseID: nurseID}, function(err, nurse) {

		Nurse.patients.push(patientID);
		Nurse.save(function(err) {
			if(err)
				throw err;
			else
				done(req, res);
		});
	});
}

this.addDoctor = function(req, res, nurseID, objectID, doctorID, doctorName, done) {

	Nurse.findOne({nurseID: nurseID}, function(err, nurse) {

		if(err)
			throw err;
		else {

			nurse.doctorList_objectID.push(objectID);
			nurse.doctorList_ID.push(doctorID);
			nurse.doctorList_name.push(doctorName);
			nurse.save(function(err) {
				if(err)
					throw err;
				else {
					done(req, res);
				}
			});
		}
	});
}

this.removeDoctor = function(req, res, doctorID, done) {

	Nurse.findOne({nurseID: req.session.nurseID}, function(err, nurseInfo) {

		if(err)
			throw err;
		else {
			var index = nurseInfo.doctorList_ID.indexOf(doctorID);
			nurseInfo.doctorList_ID.splice(index, index+1);
			nurseInfo.doctorList_objectID.splice(index, index+1);
			nurseInfo.doctorList_name.splice(index, index+1);

			nurseInfo.save(function(err) {
			
				if(err)
					throw err;
				else {
					done(req, res);
				}
			});
		}
	});
}
