/* Load the user model */
var mongoose = require('mongoose');
var userSchema = require("../models/user");
var db = require("../models/dbConnection")
var User = db.Users.model('User', userSchema);

var ObjectID = mongoose.Types.ObjectID;

this.get_info = function(username, callback) {

	User.findOne({username: username}, function(err,  userInfo) {
		if(err)
			throw err;
		else
			callback(userInfo);
	});
};

this.getEncounters = function(req, res, done) {

	Encounter.findOne({username: req.session.username}, function(err, encounterPage) {
		if(err)
			throw err;
		else {
			var encounterList = [];
			/* Go through and find all encounters */
			for(encounter in encounterPage) {
				if(encounter.indexOf("assessment") != -1) {
					var assessment = encounterPage[encounter];
					for(var i = 0; i < assessment.length; i++) {
						encounterList.push(assessment[i]);
					}
				}
			}
			done(req, res, encounterList);
		}
	});
}

/* Add a patient to the data base. Checks to make sure that the patient doesn't already exist */
this.add = function(req, res, primary, done) {
	
	/* Getting all the required information about the patient */
	username = req.session.username;
	patientID = req.body.patientID;
	patientName = req.body.patientName;
	patientAddress = req.body.patientAddress;
	patientState = req.body.patientState;
	patientCity = req.body.patientCity;
	patientZip = req.body.patientZip;
	patientBirthdate = req.body.patientBirthdate;
	patientGender = req.body.patientGender;
	patientEmail = req.body.patientEmail;
	accountPrimary = primary;

	/* DEBUG */
	console.log("Creating new patient:\n");
	console.log(patientID + " " + username + " " + patientName + " " + patientBirthdate + " " + patientGender);

	/* Check to see if they already have a patient under this user */
	Patient.findOne({'patientID': patientID}, function(err, user) {
		if(err) {
			done(req, res, false);
		}
		/* If the medical ID # has already been registered, a mistake has been made */	
		else if(user) {
			req.session.recentEvent = "A patient has already been registered with that medical ID number. Please check the number and try again";
			done(req, res);
		}
		else {

			/* Next, create a new encounter list for the patient */
			var newEncounter = new Encounter();

			newEncounter.username = req.session.username;
			newEncounter.patientID = req.body.patientID;
			newEncounter.tb_assessment = [];
		
			newEncounter.save(function(err) {
				if(err)
					throw err;
				else {
			
					/* If the patient is new to the user, add it */
					var newPatient = new Patient();
					newPatient.username = req.session.username;
					newPatient.patientID = patientID;
					newPatient._encounterID = newEncounter._id;
					newPatient.patientName = patientName;
					newPatient.patientAddress = patientAddress;
					newPatient.patientState = patientState;
					newPatient.patientCity = patientCity;
					newPatient.patientZip = patientZip;
					newPatient.patientBirthdate = patientBirthdate;
					newPatient.patientGender = patientGender;
					newPatient.patientEmail = patientEmail;
					newPatient.accountPrimary = accountPrimary;
				
					newPatient.save(function(err) {
						if(err) {
							throw err;
						}
						else {
							done(req, res, true);
						}
					});
				}
			});
		}
	});
};

/* TODO: Need to make one method for doctor's/nurse's and one for patients */
this.find = function(req, res, done) {

	username = req.session.username;

	/* Find all the patients with that name */
	Patient.find({'username': username}, function(err, patientList) {

		done(req, res, patientList);
		//res.render('patientResults', {patientList: user, user: req.session.username});
	});

};


this.find_byID = function(req, res, patientID, done) {

	/* Grab the information about that patient */
	Patient.find({'patientID': patientID}, function(err, patientInfo) {
		done(req, res, patientInfo);
	});
}


this.find_byDoctorID = function(req, res, doctorID, done) {

	/* Get all the patient's that are registered to the doctor */
	Patient.find({doctorID: doctorID}, function(err, patientList) {
		done(req, res, patientList);
	});
}

this.addRequest = function(req, res, patientID, doctorID, done) {
	console.log("PatientID: " + patientID + " DoctorID: " + doctorID);
	Patient.findOne({patientID: patientID}, function(err, patientInfo) {
		if(patientInfo.pendingID.indexOf(doctorID) == -1) {
			console.log("Here");
			patientInfo.pendingRequest = true;
			patientInfo.pendingID.push(doctorID);
		}
		patientInfo.save(function(err) {
			console.log(patientInfo[0]);
			done(req, res);
		});
	});
}

this.getRequests = function(req, res, done) {
	
	Patient.find({username: req.session.username}, function(err, patientList) {
		if(err)
			throw err;
		
		var requestList = [];
		for(var i = 0; i < patientList.length; i++) {
			if(patientList[i].pendingRequest == true) {
				for(var n = 0; n < patientList[i].pendingID.length; n++) {
					requestList.push({patientID: patientList[i].patientID, patientName: patientList[i].patientName, doctorID: patientList[i].pendingID[n]});
					}
			}
		}
		done(req, res, requestList);	
	});
}

this.addDoctor = function(req, res, patientID, doctorID, done) {

	Patient.findOne({patientID: patientID}, function(err, patient) {
		var index = patient.doctorID.indexOf(doctorID);
		console.log("INDEX: " + index);
		if(index == -1) {
			patient.doctorID.push(doctorID);
		}
		patient.save(function(err) {
			if(err)
				throw err;
			else
				done(req, res);
		});
	});
}

this.removeDoctor = function(req, res, patientID, doctorID, done) {

	Patient.findOne({patientID: patientID}, function(err, patient) {

		var index = patient.pendingID.indexOf(doctorID);
		if(index > -1)
			patient.pendingID.splice(index, 1);
		if(patient.pendingID.length == 0)
			patient.pendingRequest = false;
		
		patient.save(function(err) {
			if(err)
				throw err;
			else
				done(req, res);
		});
	});
}

this.addPrescription = function(req, res, patientID, prescriptionID, done) {

	Patient.findOne({patientID: patientID}, function(err, patient) {

		if(err)
			throw err;
		else
			patient.prescription_id.push(prescriptionID);
			patient.save(function(err) {
				if(err)
					throw err;
				else
					done(req, res);
			});
	});
};

