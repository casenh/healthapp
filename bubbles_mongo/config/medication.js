// config/passport.js





this.getInfo = function(req, res, done) {

	var mongoose = require('mongoose');
	var medicationSchema = require('../models/medication');
	var db = require("../models/dbConnection");
	var Medication = db.Medications.model('Information', medicationSchema);
	
	console.log("begin query");
	console.log(db.Medications.readyState);
	/* Grab all of the medication information from the database */
	Medication.find({}, function(err, medicationList) {
		console.log("Finished");
		if(err)
			throw err;
		else {
			console.log("End query");
			done(req, res, medicationList);
		}
	});
};
