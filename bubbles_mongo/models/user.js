// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var userSchema = mongoose.Schema({


	local : {
		username:	String,
		password:	String,
		email:		String
	},
	
	userType: String
});

/***** METHODS ******/

/* Generate the hash */
userSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
userSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

userSchema.statics.findByName = function(name, cb) {
	this.find({ 'local.email': name}, cb);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('User', userSchema);
