// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var patientSchema = mongoose.Schema({

		username:				String,
		_encounterID:			Schema.Types.ObjectId,
		patientID:				String,
		patientName:			String,
		patientAddress:			String,
		patientState:			String,
		patientCity:			String,
		patientZip:				String,
		patientBirthdate:		String,
		patientGender:			String,
		accountPrimary:			Boolean,
		doctorID:				[String],
		_subPatients_id:		[Schema.Types.ObjectId],
		subPatients_id:			String,
		pendingRequest:			Boolean,
		pendingID:				[String],

		upcomingEncounters: {
			date:		[String],
			procedure:	[String],
			doctor:		[String],
			hospital:	[String]
		},

		recentEncounters: {
			date:		[String],
			procedure:	[String],
			doctor:		[String],
			hospital:	[String]
		},

		prescription_id:	[String],
});

/***** METHODS ******/

/* Generate the hash */
patientSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
patientSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('Patient', patientSchema);
