// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var oralCancerSchema = mongoose.Schema({

		parentID:				String,
		procedureType:			String,
		username:				String,
		patientID:				String,
		patientName:			String,
		nurseID:				String,
		nurseName:				String,
		doctorID:				[String],
		doctorName:				String,
		date:					String,
		start_time:				String,
		end_time:				String,
		encounterLocation:		String,		



		familyHistory:			String,
		educationLevel:			String,
		employed:				String,
		drinker:				String,
		smoker:					String,
		tobacco:				String,
		dentist:				String,
		brush_teeth:			String,
		toothpowder:			String,
		lesion:					String,
		hosptial:				String,
		high_risk:				String,
		details_logged:			Boolean
	
});

/***** METHODS ******/

/* Generate the hash */
oralCancerSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
oralCancerSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
//module.exports = mongoose.model('oralCancerEncounter', oralCancerSchema);
module.exports = oralCancerSchema;
