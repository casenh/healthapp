// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var doctorSchema = mongoose.Schema({

		username:				String,
		doctorID:				String,
		doctorName:				String,
		doctorAddress:			String,
		doctorState:			String,
		doctorCity:				String,
		doctorZip:				String,
		doctorHospital:			String,
		doctorSpecialty:		String,
		doctorBirthdate:		String,
		doctorGender:			String,
		patients:				[String],
		encounters:				[String], //Contains patient id's where the doctor only has
		doctorAppointments:		[String], //access specific encounters
		nurses:					[String],
		nurseRequestList:		[String],
		nurseRequest:			Boolean
		
});

/***** METHODS ******/

/* Generate the hash */
doctorSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
doctorSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('doctor', doctorSchema);
