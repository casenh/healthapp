// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var medicationSchema = mongoose.Schema({

	name:				String,
	dosageInfo:	{
		type:			[String],
		dosage:			[String]
	},
	applicationID:		String,
	activeIngredient:	String,
	information:		String

});

/**** METHODS ******/

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('Information', medicationSchema);
