// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var tb_assessment_schema = require('./tb_assessment');
var oral_cancer_assessment_schema = require('./oral_cancer_assessment');
var ophthalmology_assessment_schema = require('./ophthalmology_assessment');

/* Define the schema for our model */
var encounterSchema = mongoose.Schema({

		username:						String,
		patientID:						String,
		doctorID:						[String],
		tb_assessment:					[tb_assessment_schema],
		oral_cancer_assessment:			[oral_cancer_assessment_schema],
		ophthalmology_assessment:		[ophthalmology_assessment_schema],

	
});

/**** METHODS ******/

/* Generate the hash */
encounterSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
encounterSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('Encounter', encounterSchema);
