// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var nurseSchema = mongoose.Schema({

		username:				String,
		nurseID:				String,
		nurseName:				String,
		nurseAddress:			String,
		nurseState:				String,
		nurseCity:				String,
		nurseZip:				String,
		nurseHospital:			String,
		nurseBirthdate:			String,
		nurseGender:			String,
		doctorList_objectID:	[String],
		doctorList_ID:			[String],
		doctorList_name:		[String]
});

/***** METHODS ******/

/* Generate the hash */
nurseSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
nurseSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('Nurse', nurseSchema);
