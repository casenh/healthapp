// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var ophthalmologySchema = mongoose.Schema({

	parentID:				String,
	procedureType:			String,
	username:				String,
	patientID:				String,
	patientName:			String,
	nurseID:				String,
	nurseName:				String,
	doctorID:				[String],
	doctorName:				String,
	date:					String,
	start_time:				String,
	end_time:				String,
	encounterLocation:		String,


	/* Symptoms	*/
	sudden_vision_loss:				String,
	gradual_vision_loss:			String,
	pain:							String,
	tearing:						String,
	itching:						String,
	redness:						String,
	discharge:						String,
	double_vision:					String,
	headache:						String,
		
	/* Medical History */
	glaucoma:						String,
	dry_eyes:						String,
	uveitis:						String,
	allergic_eye_disease:			String,
	vernal_catarrh:					String,
	glasses:						String,
	diabetic:						String,
	hypertensive:					String,
	hiv_positive:					String,
	tb_positive:					String,
	meningitis_positive:			String,

	/* Eye Medication */
	brimondinine:					String,
	pilocarpine:					String,
	prednisolone_1per:				String,
	prednisolone_12per:				String,
	flourometholone_neomycin:		String,
	tetracycline:					String,
	chloramphenicol:				String,
	tears:							String,
	antihistamine_drops:			String,
	sodium_cromoglycate_drops:		String,

	/* Previous Surgeries */	
	cataract:						String,
	pteryglum_excision:				String,
	trabeculectumy:					String,
	cryotherapy:					String,
	conjunctival_tumar_excision:	String,
	pan_retinal_laser_treatment:	String,
	macular_laser_treatment:		String,
	yag_laser_treatment:			String,
	cornea_transplant:				String,
	evisceration:					String,
	enucleation:					String,

	/* Family History */
	glaucoma_family:				String,
	cataract_family:				String,

	/* Information */
	right_acuity:					String,
	left_acuity:					String,
	right_acuity_pinhole:			String,
	left_acuity_pinhole:			String,
	right_ocular_pressure:			String,
	left_ocular_pressure:			String,

	/* Globe/Orbit Information */
	right_proptosis:				String,
	right_swelling:					String,
	right_reduced_movement:			String,
	right_squint:					String,
	right_retropulsion_resistance:	String,
	right_tumour:					String,
	left_proptosis:					String,
	left_swelling:					String,
	left_reduced_movement:			String,
	left_squint:					String,
	left_retropulsion_resistance:	String,
	left_tumour:					String,
	
	/* Eyelid Information */
	right_trichiasis:				String,
	right_everted:					String,
	right_close:					String,
	right_swelling_eyelid:			String,
	right_pouting_meibomian:		String,
	right_stye:						String,
	right_chalazion:				String,
	right_tumour_eyelid:			String,
	right_herpes:					String,
	left_trichiasis:				String,
	left_everted:					String,
	left_close:						String,
	left_swelling_eyelid:			String,
	left_pouting_meibomian:			String,
	left_stye:						String,
	left_chalazion:					String,
	left_tumour_eyelid:				String,
	left_herpes:					String,

	/* Pupils */
	right_round_reactive:			String,
	right_afferent_pupillary:		String,
	right_irregular:				String,
	right_small:					String,
	right_large:					String,
	right_unreactive:				String,
	right_posterior_synichia:		String,
	left_round_reactive:			String,
	left_afferent_pupillary:		String,
	left_irregular:					String,
	left_small:						String,
	left_large:						String,
	left_unreactive:				String,
	left_posterior_synichia:		String,

	/* Conjunctiva */
	right_hyperaemia:				String,
	right_chemosis:					String,
	right_discharge:				String,
	right_follicles:				String,
	right_pappillary_reaction:		String,
	right_pterygium:				String,
	right_tumour_conj:				String,
	left_hyperaemia:				String,
	left_chemosis:					String,
	left_discharge:					String,
	left_follicles:					String,
	left_pappillary_reaction:		String,
	left_pterygium:					String,
	left_tumour_conj:				String,

	/* Cornea */
	right_reduced_sensation:		String,
	right_ulcer:					String,
	right_scar:						String,
	right_erosion:					String,
	right_dry:						String,
	right_keratopathy:				String,
	right_oedema:					String,
	left_reduced_sensation:			String,
	left_ulcer:						String,
	left_scar:						String,
	left_erosion:					String,
	left_dry:						String,
	left_keratopathy:				String,
	left_oedema:					String,

	details_logged:					Boolean

});

/***** METHODS ******/

/* Create the model for users and expose it to the app */
module.exports = ophthalmologySchema;
