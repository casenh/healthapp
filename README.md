OVERVIEW
========

This respository contains working versions of an example NodeJS medical
application written in javascript and HTML. The director structure is as
follows:

./base_app -- This is the default application. It uses MongoDB to maintain
              information about different doctors and patients. This is the
			  only version of the app capable of running independently.

./bubbles -- A modified version of the application tailored to run
			 as a trusted app for the Bubbles Platform. All information
			 is safed in JSON text files.

./bubbles_mongo -- A modified version of the application tailored to run
				   as a trusted app for the Bubbles Platform. All information
			 	   is safed in MongoDB.


INSTALLATION
============

*NOTE: THE './base_app' VERSION OF THE APP IS THE ONLY VERSION OF THE APP*
*CAPABLE OF RUNNING INDEPENDENTLY. THE REST REQUIRE THE BUBBLES PLATFORM.*
*SCROLL DOWN FOR INSTRUCTIONS ON BUBBLES INSTALLATION.*

Dependencies
------------

# NodeJS

The Node binaries from the Ubuntu source repositories may cause errors
with the application. The following methods are recommended for installing
the Node:

1) curl -sL https://deb.nodesource.com/setup | sudo bash -
2) sudo apt-get install nodejs
3) sudo apt-get install mongodb

If that doesn't work for installing node, then try:

1) sudo apt-get install python-software-properties
2) sudo apt-add-repository ppa:chris-lea/node.js
3) sudo apt-get update
4) sudo apt-get install nodejs

# Node Package Manager (NPM)

1) sudo apt-get install npm

# MongoDB

1) sudo apt-get install mongodb

Quick Install
-------------

Run  the 'init_base.sh' script to install all Node dependencies and
populate the MongoDB with test data. 

Initialization
--------------

First, initialize the database information for the app by running the init.sh
script in the top directory.  All Node dependencies are installed locally to
package folders by NPM. To initialize the apps, change to the directories and
run:
	
	npm install


Running
-------

To start the application, run:
	
	npm start

Visit localhost:3000 to interact with the application.



BUBBLES INSTALLATION
====================

Dependencies
------------

Two other repositories are required to run the bubbles platform. Clone the following:
	
	http://sparkc.ece.utexas.edu/jimmy/bubble-trusted-viewer.git
	http://sparkc.ece.utexas.edu/jimmy/bubble_launcher.git

View the README in each of the following for any required dependencies of these.


Initialization
--------------

To run the application within a bubble, copy the version of the application
(not including the base version) you wish to deploy into the
./bubbles-trusted-viewer/test/ folder.


Running
-------

To launch to bubbles platform, first execute the bubbles backend in the bubble_launcher
repository:

	sudo ./run-daemon process

Next, launch the trusted viewer in bubble-trusted-viewer, which will populate
the approprate bubbles for you. Pass in the version of the app you
wish to run (i.e. healthapp_trusted), or leave it empty for the default
debugging apps:

	sudo ./start.sh [App Version]










