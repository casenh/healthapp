#!/bin/bash

cd ./base_app/

# Install dependencies
npm install

# Populate the database with fake information
cd ./populate_db/
./populate.py
