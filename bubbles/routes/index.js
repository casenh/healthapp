var express = require('express');
var querystring = require('querystring');
var session = require('express-session');
var fs = require('fs');
var request = require('request');

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	
	/* Grab the user's information from Global Parameters set by the viewer */
	session.username = process.env.BIEWER_USER

	/* Take them to the profile page */
	res.redirect('/profile');
});

/* GET quit page for exiting */
router.get('/quit', function(req, res) {
	/* Exit after the gather */
	setTimeout(function() {
		process.exit(0);
	}, 2000);
});

/* GET delete page for removing bubble */
router.get('/delete_post', function(req, res) {

	var id = 'http://localhost:8080/api/admin/bubbles/' + process.env.BUBBLE_ID;

	var options = {
		url: id,
		method: 'DELETE',
		headers: {
		     'Authorization': 'Basic ' + new Buffer('admin' + ':' + 'pass').toString('base64')
	   }
	};

	request(options, function(err, res, body) {
		if(err && res.statusCode == 200) {
			console.log("ERR");
		}
		console.log("Success");
	});

	res.render('delete_success', {});	
});

/* GET wait page for gather requests */
router.get('/wait', function(req, res) {

	/* Make sure to grab the path that the gather came from */
	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	res.render('wait', {backURL: url.path});
});

/* GET user_info page */
router.get('/user_info', function(req, res) {
	var data = process.env.BUBBLES_STORAGE + '/shared/data.json';
	var result = require(data);
	res.render('user_info', {username: result.username, name: result.name, patientID: result.patientID, email: result.email});
});

/* GET encounter_info page */
router.get('/encounter_info', function(req, res) {
	var data = process.env.BUBBLES_STORAGE + '/shared/data.json';
	var result = require(data);
	res.render('encounter_info', {username: result.username, type: result.type, date: result.date, patientID: result.patientID, doctorName: result.doctorName});
});

/* GET prescription_info page */
router.get('/prescription_info', function(req, res) {
	var data = process.env.BUBBLES_STORAGE + '/shared/data.json';
	var result = require(data);
	res.render('prescription_info', {username: result.username, name: result.name, date: result.date, brand: result.brand, doctorName: result.doctorName});
});

/* POST add data to a bubble. The form for the file is saved as a JSON */
router.post('/add-data', function(req, res) {

	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	var type = url.type;

	var results = '';
	req.on('data', function(data) {
		results += data.toString();
		console.log(results);
	});
	req.on('end', function() {
		var post = querystring.parse(results);
		var file = process.env.BUBBLES_STORAGE + '/shared/data.json';
		fs.readFile(file, function(err, data) {
			if(err)
				console.log("ERR READ");
			else {

				fileData = JSON.parse(data);
				if(type == "Encounter")
					fileData.Encounter.push(post);
				else if(type == "Prescription")
					fileData.Prescription.push(post);
				else if(type = "Information")
					fileData.Information.push(post);

				fs.writeFile(file, JSON.stringify(fileData), function(err) {
					if(err)
						console.log("ERR WRITE");
				});
			}
		});
		res.render('wait', {backURL: '/quit'});
	});
});

/* POST adding a bubble */
router.get('/add-bubble', function(req, res) {
	/* Create a random name for the bubble */
	var name = Math.random().toString(36).substring(7);

	var options = {
		uri: 'http://localhost:8080/api/admin/bubbles',
		method: 'POST',
		json: {
			"name": name
		},
		headers: {
		     'Authorization': 'Basic ' + new Buffer('admin' + ':' + 'pass').toString('base64')
	   }
	};

	request(options, function(err, response, body) {
		if(err && res.statusCode == 200) {
			console.log("ERR");
		}
		console.log("Success");
	});

	res.render('wait', {backURL: '/profile'});
});

/* GET profile page */
router.get('/profile', function(req, res) {

	/* Make a gather request to collect all encounter  */
	//Set the appropriate headers
	if(process.env.COMMAND != "gather") {
		var obj = JSON.stringify(["run", "--command", "gather"])
		res.setHeader("x-biewer-gather-command", obj);
		res.setHeader("x-biewer-gather-context", "result");
		res.setHeader("x-biewer-gather-wait", "/wait?path=/profile")
		res.render('bubbles' , {});
	}
	else {
		var file = process.env.BUBBLES_STORAGE + '/shared/data.json';
		
		fs.readFile(file, function(err, data) {
			if(err) {
				/* Check if there are any empty bubbles */
				if(err.errno == 34) {
					var bubble_template = { Information:[], Encounter:[], Prescription:[] }
					fs.writeFile(file, JSON.stringify(bubble_template), function(err) {
						if(err)
							console.log("WRITE ERR");
						else {
							res.send(bubble_template);
						}
					});	
				}
			}
			else {
				result = JSON.parse(data);
				console.log(result);
				res.json(result);
			}
			/* Exit after Gather */
			setTimeout(function() {
				process.exit(0);
			}, 2000);
		});
	}
});

/* POST for patient search of doctors */
router.get('/searchDoctors', function(req, res) {

	/* Make a gather request to collect all encounter information */
	//Set the appropriate headers
	if(process.env.COMMAND != "gather") {
		var obj = JSON.stringify(["run", "--command", "gather"])
		res.setHeader("x-biewer-gather-command", obj);
		res.setHeader("x-biewer-gather-context", "result");
		res.setHeader("x-biewer-gather-wait", "/wait?path=/searchDoctors")
		res.render('searchDoctors' , {});
	}
	else {
		var file = process.env.BUBBLES_STORAGE + '/shared/data.json';
		//var result = require(data);
		//console.log("RESULT: " + result);
		
		fs.readFile(file, function(err, data) {
			if(err || !data) {
				console.log("ERR");
				res.json({"norender": "true"});
			}
			else {
				result = JSON.parse(data);
				if (result.Doctor == "True")
					res.json(result);
				else
				res.json({});
			}
			/* Exit after Gather */
			setTimeout(function() {
				process.exit(0);
			}, 2000);
		});
	}
});

/* GET page for searching the medication database */
router.get('/searchMedication', function(req, res) {

	var file = __dirname + '/medication_dump.json';

	fs.readFile(file, function(err, data) {
		if(err || !data) {
			console.log("ERR");
			res.json({"norender": "true"});
		}
		else {
			result = JSON.parse(data);
			console.log(result[0]);
			res.render('searchMedication', {medicationList: result});
		}

	});
});

/* GET patient page */
router.get('/patient', function(req, res) {

	/* Make a gather request to collect all encounter information */
	//Set the appropriate headers
	if(process.env.COMMAND != "gather") {
		var obj = JSON.stringify(["run", "--command", "gather"])
		res.setHeader("x-biewer-gather-command", obj);
		res.setHeader("x-biewer-gather-context", "result");
		res.setHeader("x-biewer-gather-wait", "/wait?path=/patient")
		res.render('searchPatients' , {});
	}
	else {
		var file = process.env.BUBBLES_STORAGE + '/shared/data.json';
		fs.readFile(file, function(err, data) {
			if(err || !data) {
				console.log("ERR");
				res.json({"norender": "true"});
			}
			else {
				result = JSON.parse(data);
				console.log(result);
				res.json(result);
			}
			/* Exit after Gather */
			setTimeout(function() {
				process.exit(0);
			}, 2000);
		});
	}
});

/* GET prescriptions page */
router.get('/managePrescriptions', function(req, res) {

	/* Make a gather request to collect all encounter information */
	//Set the appropriate headers
	if(process.env.COMMAND != "gather") {
		var obj = JSON.stringify(["run", "--command", "gather"])
		res.setHeader("x-biewer-gather-command", obj);
		res.setHeader("x-biewer-gather-context", "result");
		res.setHeader("x-biewer-gather-wait", "/wait?path=/managePrescriptions")
		res.render('managePrescriptions' , {});
	}
	else {
		var file = process.env.BUBBLES_STORAGE + '/shared/data.json';
		fs.readFile(file, function(err, data) {
			if(err || !data) {
				console.log("ERR");
				res.json({"norender": "true"});
			}
			else {
				result = JSON.parse(data);
				console.log(result);
				res.json(result);
			}
			/* Exit after Gather */
			setTimeout(function() {
				process.exit(0);
			}, 2000);
		});
	}
});

/* GET manageencounter page */
router.get('/manageEncounters', function(req, res) {

	var options = {
		uri: 'http://localhost:8080/api/admin/bubbles',
		method: 'GET',
		headers: {
		     'Authorization': 'Basic ' + new Buffer('admin' + ':' + 'pass').toString('base64')
	   }
	};
});

/* GET adding information to bubbles */
router.get('/add-encounter', function(req, res) {
	res.render('add-encounter', {});
});

router.get('/add-information', function(req, res) {
	res.render('add-information', {});
});

router.get('/add-prescription', function(req, res) {
	res.render('add-prescription', {});
});


/* GET admin page */
router.get('/addEncounter', function(req, res) {

	var options = {
		uri: 'http://localhost:8080/api/admin/bubbles',
		method: 'POST',
		json: {
			"name": "Test"
		},
		headers: {
		     'Authorization': 'Basic ' + new Buffer('admin' + ':' + 'pass').toString('base64')
	   }
	};

	request(options, function(err, res, body) {
		if(err && res.statusCode == 200) {
			console.log("ERR");
		}
		console.log("Success");
	});

	res.render('encounter_success', {});	
});

/* POST find patient page */
router.post('/addEncounter', function(req, res) {

	var options = {
		uri: 'http://localhost:8080/api/admin/bubbles',
		method: 'POST',
		json: {
			"name": "Test"
		},
		auth: ("admin", "pass")
	};

	request(options, function(err, res, body) {
		if(err && res.statusCode == 200) {
			console.log("ERR");
		}
		res.render('/encounter_success', {});
	});
});


module.exports = router;
