#!/usr/bin/env python

import random
import json
import subprocess

NAMES = './data/prescription_names.txt'

if __name__ == '__main__':
	# First, open all the necessary files and populate the arrays
	nameFile = open(NAMES, 'r')
	nameArray = []
	for name in nameFile:
		nameArray.append(name[:-1])
	
	#Now print the results to a file
	json_dump_file = open("prescriptionJSON.txt", 'w')
	random.seed(100)
	for n in range(0,1000):

            #Give each patient a random number of prescriptions between 1 and 5
            numPrescriptions = random.randint(1,5)
            for i in range(1,numPrescriptions):
		#Get the name
		temp = random.randint(0,len(nameArray)-1)
	    	name = nameArray[temp]

		#Get the username
		username = "Patient" + str(n)

		#Get the patientID
		patientID = str(n)

		#Get the strength
                temp = random.randint(0,1)
                strength = "low"
                if temp == 0:
                    strength = "medium"
                else:
                    strenght = "high"

                #Get the form
                temp = random.randint(0,1)
                form = "pill"
                if temp == 0:
                    form = "liquid"
                if temp == 1:
                    form = "spray"

                #Get the route
                route = "oral"
                if temp == 1:
                    route = "nasal"
                   
                #Get the quantity
                temp = random.randint(15,30)
                quantity = str(temp) + " start_days"

                #Get the dosage
                temp = random.randint(5,20)
                dosage = str(temp) + " mg"

                #Get the frequency
                temp = random.randint(1,3)
                frequency = str(temp) + " times daily"

                #Get the instructions
                instructions = "Here is where a doctor would write the the patient, providing detailed instruction on when and how to take the prescription."

                #Get the refills
                temp = random.randint(0,5)
                refills = str(temp)

                #Get the start date
                start_month = random.randint(1,12)
		if start_month < 10:
			start_month = "0" + str(start_month)
		else:
			start_month = str(start_month)
		start_day = random.randint(1, 30)
		if start_day < 10:
		    start_day = "0" + str(start_day)
		else:
		    start_day = str(start_day)
		start_year = "2014"

                #Get the end date
                end_month = random.randint(1,12)
		if end_month < 10:
			end_month = "0" + str(end_month)
		else:
			end_month = str(end_month)
		end_day = random.randint(1, 30)
		if end_day < 10:
		    end_day = "0" + str(end_day)
		else:
			end_day = str(end_day)
		end_year = "2014"

                #Get the doctor ID
                temp = random.randint(1001, 2000)
                doctorID = str(temp)

                #Get the notes
                notes = "These are notes that the doctor may want to make when taking the prescription"

                #Get the name of the pharmacy
                temp = random.randint(0,3)
                pharmacy = "Hospital"
                if temp == 0:
                    pharmacy = "Walgreens"
                if temp == 1:
                    pharamcy = "Walmart"
                if temp == 2:
                    pharacy = "CVS"

                #Was it dispensed in office
                temp = random.randint(0,1)
                inoffice = "Yes"
                if temp == 0:
                    inoffice = "No"

                #Name brand necessary
                temp = random.randint(0,1)
                namebrand = "No"
                if temp == 0:
                    namebrand = "Yes"

                #Outside pharamcy
                temp = random.randint(0,1)
                rxoutside = "No"
                if temp == 0:
                    rxoutside = "Yes"

                #OTC
                otc = "No"

                #DEA Number
                deanum =  str(random.randint(1000000,4000000))


		#Create the JSON
                json_dump = json.dumps([{'username': username, 'patientID': patientID, 'name': name, 'strength': strength, 'form': form, 'route': route, 'quantity': quantity, 'dosage': dosage, 'frequency': frequency, 'instructions': instructions, 'refills': refills, 'start_date': (start_month + "/" + start_day + "/" + start_year), 'end_date': (end_month + "/" + end_day + "/" + end_year), 'doctorID': doctorID, 'notes': notes, 'pharmacy': pharmacy, 'dispense_inOffice': inoffice, 'brand_necessary': namebrand, 'rx_outside': rxoutside, 'otc': otc, 'print_deanum': deanum}])
		json_dump_file.write(json_dump[1:-1] + '\n')

        # Now import the data into the database
	json_dump_file.close()
	subprocess.call(['mongoimport', '--db', 'prescriptions', '--collection', 'prescriptions', '--type', 'json', '--file', './prescriptionJSON.txt'])
