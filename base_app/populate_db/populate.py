#!/usr/bin/env python

import subprocess

subprocess.call(['mongoimport', '--db', 'entities', '--collection', 'patients', '--type', 'json', '--file', './JSON/patients.json'])
subprocess.call(['mongoimport', '--db', 'entities', '--collection', 'doctors', '--type', 'json', '--file', './JSON/doctors.json'])
subprocess.call(['mongoimport', '--db', 'users', '--collection', 'users', '--type', 'json', '--file', './JSON/users.json'])
subprocess.call(['mongoimport', '--db', 'encounters', '--collection', 'encounters', '--type', 'json', '--file', './JSON/encounters.json'])
subprocess.call(['mongoimport', '--db', 'prescriptions', '--collection', 'prescription', '--type', 'json', '--file', './JSON/prescriptions.json'])
subprocess.call(['mongoimport', '--db', 'medication', '--collection', 'information', '--type', 'json', '--file', './JSON/medication.json'])
