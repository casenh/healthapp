// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var tbAssessmentSchema = new mongoose.Schema({

		parentID:				String,
		procedureType:			String,
		username:				String,
		patientID:				String,
		patientName:			String,
		nurseID:				String,
		nurseName:				String,
		doctorID:				[String],
		doctorName:				String,
		date:					String,
		start_time:				String,
		end_time:				String,
		encounterLocation:		String,
		cough:					String,
		fever:					String,
		poor_weight_gain:		String,
		night_sweats:			String,
		fatigue:				String,
		chest_pain:				String,
		contact:				String,
		afb_results:			String,
		anti_biotics:			String,
		details_logged:			Boolean
});

tbAssessmentSchema.method('create', function(req, res, parentID, done) {
	this.parentID = parentID;
	this.procedureType = "TB Assessment";
	this.username = req.session.username;
	this.patientID = req.body.patientID;

	/* Now log the correct information based on who is creating the appointment */
	if(req.session.userType == "Patient") {
		this.nurseID = null;
		this.doctorID = null;
	}
	else if(req.session.userType == "Nurse") {
		this.nurseID = req.session.nurseID;
		this.doctorID = [req.body.doctorID];
	}
	else if(req.session.userType == "Doctor") {
		this.nurseID = null;
	   	this.doctorID = [req.session.doctorID];
	}
	
	this.date = req.body.encounterDate;
	this.start_time = req.body.startTime;
	this.end_time = req.body.endTime;
	this.encounterLocation = req.body.encounterLocation;
	done(req, res);
});

tbAssessmentSchema.method('update', function(req, res, done) {

	/* Now populate the data from the form */	
	if(req.body.cough == "on")
		this.cough = "Yes";
	else
		this.cough = "No";

	if(req.body.fever == "on")
		this.fever = "Yes";
	else
		this.fever = "No";

	if(req.body.poor_weight_gain == "on")
		this.poor_weight_gain = "Yes";
	else
		this.poor_weight_gain = "No";
	
	if(req.body.night_sweats == "on")
		this.night_sweats = "Yes";
	else
		this.night_sweats = "No";

	if(req.body.fatigue == "on")
		this.fatigue = "Yes";
	else
		this.fatigue = "No";

	if(req.body.chest_pain == "on")
		this.chest_pain = "Yes";
	else
		this.check_pain = "No";

	this.contact = req.body.contact;
	this.afb_results = req.body.afb_results;
	this.anti_biotics = req.body.anti_biotics;
	this.details_logged = true;

	done(req, res);
});

module.exports = mongoose.model('TB_Assessment', tbAssessmentSchema);
