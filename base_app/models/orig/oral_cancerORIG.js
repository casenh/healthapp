// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var oralCancerSchema = mongoose.Schema({

		username:				String,
		patientID:				String,
		familyHistory:			Boolean,
		educationLevel:			String,
		employed:				Boolean,
		drinker:				String,
		smoker:					String,
		tobacco:				String,
		dentist:				Boolean,
		brush_teeth:			Boolean,
		toothpowder:			Boolean,
		lesion:					Boolean,
		hosptial:				String,
		high_risk:				Boolean
	
});

/***** METHODS ******/

/* Generate the hash */
oralCancerSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
oralCancerSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('oralCancerEncounter', oralCancerSchema);
