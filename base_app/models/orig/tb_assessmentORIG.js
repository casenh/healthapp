// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var tbAssessmentSchema = mongoose.Schema({

		procedureType:			String,
		username:				String,
		patientID:				String,
		doctorName:				String,
		date:					String,
		time:					String,
		encounterLocation:		String,
		cough:					Boolean,
		fever:					Boolean,
		poor_weight_gain:		Boolean,
		night_sweats:			Boolean,
		fatigue:				Boolean,
		chest_pain:				Boolean,
		contact:				Boolean,
		afb_results:			Boolean,
		anti_biotics:			Boolean,
	
});

/***** METHODS ******/

/* Generate the hash */
tbAssessmentSchema.methods.generateHash = function(password) {

	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/* Checking if password is valid */
tbAssessmentSchema.methods.validPassword = function(password) {
	
	return bcrypt.compareSync(password, this.local.password);
};

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('tbAssessmentEncounter', tbAssessmentSchema);
