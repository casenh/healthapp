// models/user.js

/* Get mongodb address and port from env if using docker, or
 * default to '+ address + ':' + port + ':27017 */
var process = require('process');
var address = process.env.MONGODB_PORT_27017_TCP_ADDR;
var port = process.env.MONGODB_PORT_27017_TCP_PORT;
address = (address == undefined)? "localhost" : address;
port = (port == undefined)? "27017" : port;
var mongoose = require('mongoose')

/* Define our DB object which holds all our connections. Only need
 * to create the connections once */
var db = {
	Encounter : mongoose.connection,
	Entities:	mongoose.connection,
	Prescriptions: mongoose.connection,
	Medications: mongoose.connection,
	Users: mongoose.connection,
	Session: mongoose.connection
}

/* Create our connections */
db.Encounter = mongoose.createConnection('mongodb://'+ address + ':' + port + '/encounters')
db.Entities = mongoose.createConnection('mongodb://'+ address + ':' + port + '/entities')
db.Prescriptions = mongoose.createConnection('mongodb://'+ address + ':' + port + '/prescriptions')
db.Medications = mongoose.createConnection('mongodb://'+ address + ':' + port + '/medication')
db.Users = mongoose.createConnection('mongodb://'+ address + ':' + port + '/users')
db.Session = mongoose.createConnection('mongodb://'+ address + ':' + port + '/session')

/* Create the model for users and expose it to the app */
module.exports = db
