// models/user.js

/* Load what we need */
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

/* Define the schema for our model */
var prescriptionSchema = mongoose.Schema({

	username:			String,
	patientID:			String,
	name:				String,
	strength:			String,
	form:				String,
	route:				String,
	quantity:			String,
	dosage:				String,
	frequency:			String,
	instructions:		String,
	refills:			String,
	start_date:			String,
	end_date:			String,
	doctorID:			String,
	notes:				String,
	pharmacy:			String,
	dispense_inOffice:	String,
	brand_necessary:	String,
	rx_outside:			String,
	otc:				String,
	print_deaNum:		String	
});

/* Create the model for users and expose it to the app */
module.exports = mongoose.model('Prescription', prescriptionSchema);
