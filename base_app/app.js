var cluster = require('cluster');

/* Spawn multiple node processes for scaling */
if (cluster.isMaster) {

	
	var numCPUs = require('os').cpus().length;
	var http = require('http');

	for(var i = 0; i < process.env.NUM_CPUS; i++) {
		cluster.fork();
	}

	cluster.on('exit', function(worker, code, signal) {
		//console.log('worker ' + worker.process.pid + ' died');
	});

}
else {

	var express = require('express');
	var path = require('path');
	var favicon = require('static-favicon');
	var logger = require('morgan');
	var cookieParser = require('cookie-parser');
	var bodyParser = require('body-parser');
	var passport = require('passport');
	var mongoose = require('mongoose');
	var flash = require('connect-flash');
	var session = require('express-session');
	var MongoStore = require('connect-mongo')(session);
	var angular = require('angular');
	var db = require('./models/dbConnection');
	
	
	var routes = require('./routes/index');
	var users = require('./routes/users');
	
	require('./config/passport')(passport);
	
	var app = express();
	
	var port = process.env.PORT || 3000;
	
	// view engine setup
	app.set('views', path.join(__dirname, 'views'));
	app.set('view engine', 'jade');
	
	/* Set the port */
	app.set('port', port);
	
	app.use(favicon());
	/* Toggle Debug Logs */
	//app.use(logger('dev'));
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded());
	app.use(cookieParser());
	app.use(express.static(path.join(__dirname, 'public')));
	
	/* Required for passport */
	app.use(passport.initialize());
	app.use(passport.session());
	app.use(flash());

	/* Set up the backend store for sessions */
	app.use(session({ 
		store: new MongoStore({ mongooseConnection: db.Session}),	
		secret: 'thisisasecret' 
	}));
	
	app.use('/', routes);
	app.use('/users', users);
	
	/// catch 404 and forward to error handler
	app.use(function(req, res, next) {
	    var err = new Error('Not Found');
	    err.status = 404;
	    next(err);
	});
	
	/// error handlers
	
	// development error handler
	// will print stacktrace
	if (app.get('env') === 'development') {
	    app.use(function(err, req, res, next) {
	        res.status(err.status || 500);
	        res.render('error', {
	            message: err.message,
	            error: err
	        });
	    });
	}
	
	// production error handler
	// no stacktraces leaked to user
	app.use(function(err, req, res, next) {
	    res.status(err.status || 500);
	    res.render('error', {
	        message: err.message,
	        error: {}
	    });
	});

	module.exports = app
};


