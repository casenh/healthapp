// config/passport.js
/* Load the user model */
var mongoose = require('mongoose');
var tbAssessmentSchema = require("../models/tb_assessment");
//var db = mongoose.createConnection('mongodb://localhost/encounters');
var db = require("../models/dbConnection")

/* Create each of the counters that we will be adding to the database */
var tbAssessmentEncounter = db.Encounter.model('TB_Assessment', tbAssessmentSchema);


this.find = function(req, res, returnList) {

	tbAssessmentEncounter.find({username: req.session.username}, function(err, encounterList) {
		if(err)
			throw err;
		else {
			returnList.push(encounterList);
			return;
		}
	});
}

/* Expose the functionality */
this.add = function(req, res, done) {

	var newEncounter = new tbAssessmentEncounter();
	
	/* Populate the object from the input on the site */
	newEncounter.username = req.session.username;
	newEncounter.patientID = req.body.patientID;	
	newEncounter.patientName = req.body.patientName;
	newEncounter.doctorName = req.body.doctorName;
	newEncounter.date = req.body.encounterDate;
	newEncounter.time = req.body.encounterTime;
	newEncounter.encounterLocation = req.body.encounterLocation;

	newEncounter.save(function(err) {
		if(err) {
			throw err;
		}
		else {
			tbAssessmentEncounter.find({username: req.session.username}, function(err, encounterList) {
				if(err)
					res.render('encounter', {user: req.session.username});
				if(encounterList.length > 0) {
					done(req, res, encounterList);
				}
				else {
					done(req, res, {user: req.session.username});
				}
			});
		}
	});

};


this.update = function (req, res, encounterID, done) {

	/* Create the model and pull all the data from the webpage */
	tbAssessmentEncounter.findOne({_id: encounterID}, function(err, encounter){
		encounter.username = req.session.username;
		encounter.patientID = req.body.patientID;
		if(req.body.cough != undefined)
			encounter.cough = true;
		else
			encounter.cough = false;
		if(req.body.fever != undefined)
			encounter.fever = true;
		else
			encounter.fever = false;
		if(req.body.poor_weight_gain != undefined)
			encounter.poor_weight_gain = true;
		else
			encounter.poor_weight_gain = false;
		if(req.body.night_sweats != undefined)
			encounter.night_sweats = true;
		else
			encounter.night_sweats = false;
		if(req.body.fatigue != undefined)
			encounter.night_sweats = true;
		else
			encounter.night_sweats = false;
		if(req.body.chest_pain != undefined)
			encounter.chest_pain = true;
		else
			encounter.chest_pain = false;
		encounter.contact = req.body.contact;
		encounter.afb_results = req.body.afb_results;
		encounter.anti_biotics = req.body.anti_biotics;
	
		encounter.save(function(err) {
			if(err) {
				throw err;
			}
			else {
				Encounter.find({username: req.session.username}, function(err, encounter) {
					if(err)
						done(req, res, {user: req.session.username});
					if(encounter.length > 0) {
						done(req, res, {encounterList: encounter, user: req.session.username});
					}
					else {
						done(req, res, {user: req.session.username});
					}
				});
			}
		});
	});
};
