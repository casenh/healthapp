// config/passport.js

/* Load the user model */
var mongoose = require('mongoose');
var medicationSchema = require('../models/medication');
//var db = mongoose.createConnection('mongodb://localhost/medication');
var db = require("../models/dbConnection")

/* Create each of the counters that we will be adding to the database */
var Medication = db.Medications.model('Information', medicationSchema);



this.getInfo = function(req, res, done) {

	/* Grab all of the medication information from the database */
	Medication.find({}, function(err, medicationList) {
		if(err)
			throw err;
		else {
			done(req, res, medicationList);
		}
	});
};
