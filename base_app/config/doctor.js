// config/passport.js

/* Load the user model */
var mongoose = require('mongoose');
var doctorSchema = require("../models/doctor");
//var db = mongoose.createConnection('mongodb://localhost/entities');
var db = require("../models/dbConnection")
var doctor = db.Entities.model('doctor', doctorSchema);

/* Grab each type of encounter that we have */
//var db_encounter = mongoose.createConnection('mongodb://localhost/encounters');
var encounterSchema = require('../models/encounter');
var Encounter = db.Encounter.model('Encounter', encounterSchema);
var tbAssessmentSchema = require('../models/tb_assessment');
var tbEncounter = db.Encounter.model('TB_Assessment', tbAssessmentSchema);

this.getEncounters = function(req, res, done) {
	/* TODO: can use req.session.doctorID and skip the first findOne */
	doctor.findOne({username: req.session.username}, function(err, doctorInfo) {
		if(err)
			throw err;
		Encounter.find({patientID: {$in: doctorInfo.patients}}, function(err, encounterList) {
			if(err)
				throw err;
			returnList = [];
			for(var i = 0; i < encounterList.length; i++) {
				for(encounter in encounterList[i]) {
					if(encounter.indexOf("assessment") != -1) {
						for(var n = 0; n < encounterList[i][encounter].length; n++) {
							returnList.push(encounterList[i][encounter][n]);
						}
					}
				}
			}
			done(req, res, returnList);
			//getIndividualEncounters(req, res, returnList, done);
		});
	});
}

this.getIndividualEncounters = function(req, res, returnList, done) {

	doctor.findOne({username: req.session.username}, function(err, doctorInfo) {
		var patientIDList = [];

		/* Select all the patients that the doctor cannot see their entire history */
		for(var i = 0; i < doctorInfo.encounters; i++) {
			if(doctorInfo.patients.indexOf(doctorInfo.encounters[i]) == -1)
				patientIDList.push(doctorInfo.encounters[i]);
		}

		Encounter.find({patientID: {$in: patientIDList}}, function(err, encounterList) {
			if(err)
				throw err;
			for(var i = 0; i < encounterList.length; i++) {
				for(encounter in encounterList[i]) {
					if(encounter.indexOf("assessment") != -1) {
						for(var n = 0; n < encounterList[i][encounter].length; n++) {
							if(encounterList[i][encounter][n].doctorID.indexOf(doctorInfo.doctorID) != -1)
								returnList.push(encounterList[i][encounter][n]);
						}
					}
				}
			}
			done(req, res, returnList);
		});
	});
}


/* Add a doctor to the data base. Checks to make sure that the doctor doesn't already exist */
this.add = function(req, res, done) {
	
	/* Getting all the required information about the doctor */
	username = req.session.username;
	doctorID = req.body.doctorID;
	doctorName = req.body.doctorName;
	doctorAddress = req.body.doctorAddress;
	doctorState = req.body.doctorState;
	doctorCity = req.body.doctorCity;
	doctorZip = req.body.doctorZip;
	doctorHospital = req.body.doctorHospital;
	doctorSpecialty = req.body.doctorSpecialty;
	doctorBirthdate = req.body.doctorBirthdate;
	doctorGender = req.body.doctorGender;
	doctorEmail = req.body.doctorEmail;

	/* Check to see if they already have a doctor under this user */
	doctor.findOne({'doctorID': doctorID}, function(err, user) {
		/* If the medical ID # has already been registered, a mistake has been made */	
		if(err) {
			done(req, res, false);
		}
		else {
			/* If the doctor is new to the user, add it */
			var newdoctor = new doctor();
			newdoctor.username = req.session.username;
			newdoctor.doctorID = doctorID;
			newdoctor.doctorName = doctorName;
			newdoctor.doctorAddress = doctorAddress;
			newdoctor.doctorState = doctorState;
			newdoctor.doctorCity = doctorCity;
			newdoctor.doctorZip = doctorZip;
			newdoctor.doctorHospital = doctorHospital;
			newdoctor.doctorSpecialty = doctorSpecialty;
			newdoctor.doctorBirthdate = doctorBirthdate;
			newdoctor.doctorGender = doctorGender;
			newdoctor.doctorEmail = doctorEmail;
			
			newdoctor.save(function(err) {
				if(err) {
					done(req, res, false);
				}
				else {
					done(req, res, true);
				}
			});
		}
	});
};

/* TODO: Need to make one method for doctor's/nurse's and one for doctors */
this.find = function(req, res, done) {

	username = req.session.username;

	/* Find all the doctors with that name */
	doctor.find({'username': username}, function(err, doctorList) {

		done(req, res, doctorList);
		//res.render('doctorResults', {doctorList: user, user: req.session.username});
	});

};


this.find_byID = function(req, res, doctorID, done) {

	/* Grab the information about that doctor */
	doctor.find({'doctorID': doctorID}, function(err, doctorInfo) {
		done(req, res, doctorInfo);
	});
}

this.get_doctors = function (req, res, done) {

	doctor.find({}, function(err, doctorList) {
		done(req, res, doctorList);
	});
}

this.getInfo = function(req, res, done) {

	doctor.findOne({username: req.session.username}, function(err, doctorInfo) {

		done(req, res, doctorInfo);
	});
}

this.addPatient = function(req, res, patientID, doctorID, done) {

	doctor.findOne({doctorID: doctorID}, function(err, doctor) {

		doctor.patients.push(patientID);
		doctor.save(function(err) {
			if(err)
				throw err;
			else
				done(req, res);
		});
	});
}

this.addNurseRequest = function(req, res, doctorID, done) {

	doctor.findOne({doctorID: doctorID}, function(err, doctorInfo) {
		if(doctorInfo.nurseRequestList.indexOf(req.session.nurseID) == -1) {
			doctorInfo.nurseRequestList.push(req.session.nurseID);
			doctorInfo.nurseRequest = true;
			doctorInfo.save(function(err) {
				if(err)
					throw err;
				else
					done(req, res);

			});
		}
		else {
			done(req, res);
		}
	});
}

this.getRequests = function(req, res, done) {

	doctor.findOne({doctorID: req.session.doctorID}, function(err, doctorInfo) {
		var requestList = [];
		for(var i = 0; i < doctorInfo.nurseRequestList.length; i++) {
			requestList.push({nurseID: doctorInfo.nurseRequestList[i]});
		}
		done(req, res, requestList);
	});
}

this.addNurse = function(req, res, nurseID, done) {

	doctor.findOne({doctorID: req.session.doctorID}, function(err, doctorInfo) {
		var index = doctorInfo.nurseRequestList.indexOf(nurseID);
		doctorInfo.nurseRequestList.splice(index, index+1);
		doctorInfo.nurses.push(nurseID);
		if(doctorInfo.nurseRequestList.length == 0)
			doctorInfo.nurseRequest = false;

		/* Grab the info that the nurse needs before we're done */
		var objectID = doctorInfo._id;
		var doctorID = doctorInfo.doctorID;
		var doctorName = doctorInfo.doctorName;

		doctorInfo.save(function(err) {
			if(err)
				throw err;
			else
				done(req, res, objectID, doctorID, doctorName);
		});
	});
}

this.removeNurse = function(req, res, nurseID, done) {

	doctor.findOne({doctorID: req.session.doctorID}, function(err, doctorInfo) {
		var index = doctorInfo.nurseRequestList.indexOf(nurseID);
		doctorInfo.nurseRequestList.splice(index, index+1);
		if(doctorInfo.nurseRequestList.length == 0)
			doctorInfo.nurseRequest = false;
		
		doctorInfo.save(function(err) {
			done(req, res);		
		});
	});
}
