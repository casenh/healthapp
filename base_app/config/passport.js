// config/passport.js

/* Load in what we need */
var LocalStrategy = require('passport-local').Strategy;

/* Load the user model */
var mongoose = require('mongoose');
var userSchema = require("../models/user");
//var User = mongoose.model('User', userSchema);
//var db = mongoose.createConnection('mongodb://localhost/users');
var db = require('../models/dbConnection')
var User = db.Users.model('User', userSchema);
var patientSchema = require('../models/patient');
//var db_patients = mongoose.createConnection('mongodb://localhost/entities');
var Patient = db.Entities.model('Patient', patientSchema);

/* Expose the functionality */
module.exports = function(passport) {


	/***** PASSPORT SESSION SETUP *****/

	/* Used to serialize the user for the session */
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	/* Used to deserialize the user */
	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	/***** LOCAL LOGIN *****/

	/* When a user logs in, check that they exist and their password */
	passport.use('local-login', new LocalStrategy({

		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	},
	function(req, res, password, done) {
		process.nextTick(function() {

			username = req.body.username;
			var password = req.body.password;
		
			
			/* Check to see if that user already exsists in our database */
			User.findOne({ 'local.username': username }, function(err, user) {
			
				if(err)
					return done(err);
				/* If the user exists, check that the password matches */
				if(user) {

					var password_match = user.validPassword(password);
					/* User checks out */
					if(password_match) {
						req.session.username = username;
						req.session.userType = user.userType;
						return done(null, user, req.flash('user', username));
						/*Patient.findOne({username: req.body.username}, function(err, patient) {

							if(err)
								throw err;
							else {
								//req.session.patientID = patient.patientID;
								return done(null, user, req.flash('user', username));
							}
						});*/
					}
				}
				/* If they don't exist or messed up their password, let them know */
				return done(null, false, req.flash('loginMessage', 'Incorrect username or password. Please try again.'));
			});
		});
	}));


	/***** LOCAL SIGNUP *****/
	passport.use('local-signup', new LocalStrategy({

		usernameField: 'username',
		passwordField: 'password',
		passReqToCallback: true
	},
	function(req, res, password, done) {

		process.nextTick(function() {

			var username = req.body.username;
			var password = req.body.password;
			var userType = req.body.userType;
		
			
			/* Check to see if that user already exsists in our database */
			User.findOne({ 'local.username': username }, function(err, user) {
			
				if(err)
					return done(err);

				if(user) {
					return done(null, false, req.flash('signupMessage', 'That username is already in use. Please select another.'));
				}
				else {
					var newUser = new User();

					newUser.local.username = username;
					newUser.local.password = newUser.generateHash(password);
					newUser.userType = userType;
					
					newUser.save(function(err) {
						if(err)
							throw err;
						else {
							req.session.username = username;
							req.session.userType = userType;
							return done(null, newUser, req.flash('profileMessage', username));
						}
					});
				}
			});
		});
	}));
};
