var express = require('express');
var passport = require('passport');
var Patient = require('../config/patient');
var Doctor = require('../config/doctor');
var Nurse = require('../config/nurse');
var Encounter = require('../config/encounter');
var Medication = require('../config/medication');
var Prescription = require('../config/prescription');
var querystring = require('querystring');
var router = express.Router();
var Log = require('../models/logging')

var currentUser = require('../models/currentUser');
var tbEncounter = require('../config/tb_assessment');


router.get('/log', function(req, res) {
	
	console.log(Log.collection)
	console.log(Log.time)
});

/* Make sure that a user is logged in before accessing potentially private pages */
function userCheck(req, res) {
	if(req.session.username == undefined) {
		res.render('newuser', {user: 'failed'});
		return false;
	}
	return true;
}

/* GET home page. */
router.get('/', function(req, res) {
	res.render('index' , {loginMessage: req.flash('loginMessage') });
});

/* POST for checking a user's login information */
router.post('/login', passport.authenticate('local-login', {

	successRedirect: 'profileLogin',
	failureRedirect: '/',
	failureFlash: true
}));

/* Called before the user logs on for the first time. Get the user's id number
 * to make methods later on much easier	*/
router.get('/profileLogin', function(req, res) {
	
	if(req.session.userType == "Doctor") {
		Doctor.getInfo(req, res, function(req, res, doctorInfo) {
			req.session.doctorID = doctorInfo.doctorID;
			res.redirect('/profile');
		});
	}
	else if(req.session.userType == "Nurse") {
		Nurse.getInfo(req, res, function(req, res, nurseInfo) {
			req.session.nurseID = nurseInfo.nurseID;
			res.redirect('/profile');
		});
	}
	else {
		res.redirect('/profile');
	}
});

/* GET signup page */
router.get('/newuser', function(req, res) {

	/* Render the signup page */
	res.render('newuser', {message : req.flash('signupMessge') });

});

/* POST signup page */
router.post('/adduser', passport.authenticate('local-signup', {

	successRedirect: '/initAccount',
	failureRedirect: '/failure',
	failureFlash: true
}));

/* GET initialize account */
router.get('/initAccount', function(req, res) {

	if(req.session.userType == "Patient")	
		res.render('./patientView/initAccount');
	else if(req.session.userType == "Nurse")
		res.render('./nurseView/initAccount');
	else if(req.session.userType == "Doctor")
		res.render('./doctorView/initAccount');
});

/* POST patient page */
router.post('/initAccount', function(req,res) {

	/* TODO: Add checks for failing to add user */
	if(userCheck(req,res)) {
		if(req.session.userType == "Patient") {
			Patient.add(req, res, true, function(req, res, result) {
				res.redirect('/profile');	
			});
		}
		else if(req.session.userType == "Nurse") {
			Nurse.add(req, res, function(req, res, result) {
				res.redirect('/profile');	
			});
		}
		else if(req.session.userType == "Doctor") {
			Doctor.add(req, res, function(req, res, result) {
				res.redirect('/profile');
			});
		}
	}
});

/* GET new appointment page */
router.get('/newEncounter', function(req, res) {

	if(userCheck(req,res))
		if(req.session.userType == "Patient") {
			Patient.find(req, res, function(req, res, patientList) {
				res.render('./patientView/newEncounter', {user: req.session.username, patientList: patientList});
			});
		}
		else if(req.session.userType == "Nurse") {
			res.render('./nurseView/newEncounter');
		}
		else if(req.session.userType == "Doctor") {
			res.render('./doctorView/newEncounter', {user: req.session.username});
		}

});

/* GET failure page if a user is unable to create a new account */
router.get('/failure', function(req, res) {
	res.render('newuser', {signupMessage: req.flash('signupMessage') });
});

/* Log a user out and bring them back to the login page */
router.get('/logout', function(req, res) {
	req.session.username = undefined;
	res.redirect('/');
});

/* Creates the event list that is required for the homescreen calendar */
function getEventList(encounterList, done) {

	var eventList = [];
	for(var i = 0; i < encounterList.length; i++) {

		var day = encounterList[i].date.substring(3,5);
		var month = encounterList[i].date.substring(0,2);
		var year = encounterList[i].date.substring(6,10);

		/* Convert the time to military time */

		var date = year + "-" + month + "-" + day + "T" + encounterList[i].start_time;
		eventList.push({title: encounterList[i].procedureType, start: date, patientID: encounterList[i].patientID, encounterLocation: encounterList[i].encounterLocation, patientID: encounterList[i].patientID, link_url: "/patientResults?patientID="+encounterList[i].patientID, encounterID: encounterList[i]._id, details_logged: encounterList[i].details_logged});
	}

	done(eventList);
}

/* Function for sorting out the dates of encounters */
function sortEncounters(encounterList, done) {

	
	/* First create a list of all the events that will be displayed on the calendar */
	var eventList = [];
	for(var i = 0; i < encounterList.length; i++) {

		var day = encounterList[i].date.substring(3,5);
		var month = encounterList[i].date.substring(0,2);
		var year = encounterList[i].date.substring(6,10);

		/* Convert the time to military time */

		var date = year + "-" + month + "-" + day + "T" + encounterList[i].start_time;
		eventList.push({title: encounterList[i].procedureType, start: date, patientID: encounterList[i].patientID});
	}

	var today = new Date();
	var currentDD = parseInt(today.getDate());
	var currentMM = parseInt(today.getMonth()) + 1;
	var currentYYYY = parseInt(today.getFullYear());


	/* Sort the encounter list before returning it */
	var upcomingEncounters = [];
	var	pastEncounters = [];
	if(encounterList) {
		for(var i = 0; i < encounterList.length; i++) {
			
			/* First convert the encounter time from military */
			var encounterTime = parseInt(encounterList[i].time.substring(0,2));
			var suffix = "AM";
			if(encounterTime > 12) {	
				encounterTime = encounterTime - 12;
				suffix = "PM";
			}
			encounterList[i].time = String(encounterTime) + encounterList[i].time.substring(2,5) + suffix;

			/* Sort the encounters between past and future */
			var encounterMM = parseInt(encounterList[i].date.substring(0,2));
			var encounterDD = parseInt(encounterList[i].date.substring(3,5));
			var encounterYYYY = parseInt(encounterList[i].date.substring(6,10));

			if(encounterYYYY < currentYYYY) {
				pastEncounters.push(encounterList[i]);
			}
			else if(encounterMM < currentMM) {
				pastEncounters.push(encounterList[i]);
			}
			else if(encounterDD < currentDD) {
				pastEncounters.push(encounterList[i]);
			}
			else {
				upcomingEncounters.push(encounterList[i]);
			}
		}
	}

	done(upcomingEncounters, pastEncounters, eventList);
}

/* GET profile page */
router.get('/profile', function(req, res) {
	
	/* Check that the user has signed in */
	if(userCheck(req, res)) {

		if(req.session.userType == 'Patient') {
			Patient.getEncounters(req, res, function(req, res, encounterList) {			
				getEventList(encounterList, function(eventList) {
					//Patient.find(req, res, function(req, res, patientList) {
						//Prescription.getPrescriptions(req, res, patientList, function(req, res, prescriptionList) {

							res.render('./patientView/profile', {user: req.session.username, eventList: eventList, prescriptionList: null});
						//});
					//});
				});
			});
		}
		else if(req.session.userType == "Nurse") {
			Nurse.getEncounters(req, res, function(req, res, encounterList) {
				getEventList(encounterList, function(eventList) {
					res.render('./nurseView/profile', {user:req.session.username, eventList: eventList});
				});
			});	
		}
		else if(req.session.userType == 'Doctor') {
		
			/* Search for all encounters associated with the doctor */
			Doctor.getEncounters(req, res, function(req, res, encounterList) {
				Doctor.getIndividualEncounters(req, res, returnList, function(req, res, encounterList) {
					getEventList(encounterList, function(eventList) {
						res.render('./doctorView/profile', {user: req.session.username, eventList: eventList});
					});
				});
			});
		}
	}
});

/* GET encounter page. */
router.get('/encounter', function(req, res) {

	/* Check that the user has signed in */
	if(userCheck(req, res))
		var encounterList = encounter.find(req, res, function(req, res, encounterList) {
			res.render('./patientView/encounter', encounterList);   
		});
});

/* GET patient page */
router.get('/patient', function(req, res) {

	/* Check that the user has signed in */
	if(userCheck(req, res)) {
		if(req.session.userType == "Nurse" || req.session.userType == "Patient") {
			Patient.find(req, res, function(req, res, patientList) {
				res.render('./patientView/patient', {user: req.session.username, patientList: patientList});
			});
		}
		else if(req.session.userType == "Doctor") {
				Doctor.getInfo(req, res, function(req, res, doctorInfo) {

				Patient.find_byID(req, res, doctorInfo.patients[0], function(req, res, patientList) {
					res.render('./doctorView/patient', {user: req.session.username, patientList: patientList});
				});
			});
		}
	}
});

/* POST patient page */
router.post('/addPatient', function(req,res) {

	if(userCheck(req,res)) {
		Patient.add(req, res, false, function(req, res, result) {
			res.redirect('./patient');	
		});
	}
});

/* POST find patient page */
router.post('/findPatient', function(req, res) {

	Patient.find(req, res, function(req, res, userList) {
		
		res.render('./patientView/patientResults', {patientList: userList, user: req.session.username});
		
	});
});

/* GET patient results page */
router.get('/patientResults', function(req, res) {
	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	if(req.session.userType == "Patient") {
		Patient.find_byID(req, res, url.patientID, function(req, res, patientInfo) {
	
			res.render('./patientView/patientResults', {user: req.session.username, patientInfo: patientInfo});
		});
	}
	else if(req.session.userType == "Doctor") {
		Patient.find_byID(req, res, url.patientID, function(req, res, patientInfo) {
			res.render('./doctorView/patientResults', {user: req.session.username, patientInfo: patientInfo, message: ""});	
		});
	}
});

/* GET for searching medication */
router.get('/searchMedication', function(req, res) {

	if(userCheck(req, res)) {
		Medication.getInfo(req, res, function(req, res, medicationList) {
			res.render('./searchMedication', {user: req.session.username, medicationList: medicationList});
	});
		}
});

router.post('/searchDoctors', function(req, res) {

	res.render('./patientView/searchDoctors', {user: req.session.username});
});

/* POST for patient search of doctors */
router.get('/searchDoctors', function(req, res) {
	
	Doctor.get_doctors(req, res, function(req, res, doctorList) {
	
		if(req.body.doctorState) {

			for(var i = 0; i < doctorList.length; i++) {
				//if(doctorList[i].doctorState == req.body.doctorState)
			}
			res.render('./patientView/searchDoctors', {user: req.session.username, doctorList: doctorList, cityList: null, hospitalList: null, specialtyList: null, state_selected: req.body.doctorState});
		}
		else {
			res.render('./patientView/searchDoctors', {user: req.session.username, doctorList: doctorList, cityList: null, hospitalList: null, specialtyList: null, state_selected: req.body.doctorState});

		}
	});
});

/* POST begin a new encounter */
router.get('/fillDetails', function(req, res) {
	
	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	var encounterType = url.encounterType;
	var patientID = url.patientID;
	var encounterID = url.encounterID;

	/* First see if we find the patient in the database */
	Patient.find(req, res, function(req, res, userList) {
		
		/* If we get one, see what encounter we're doing */
		if(encounterType == 'Oral Cancer') {
			res.render('./questionnaireForms/oral_cancer_questionnaire', {user: req.session.username});
		}
		else if(encounterType == 'TB Assessment') {
			res.render('./questionnaireForms/tb_questionnaire', {user: req.session.username, patientID: patientID, encounterID: encounterID});
		}
		else if(encounterType == 'ophthalmology') {
			res.render('./questionnaireForms/ophthalmology_questionnaire', {user: req.session.username});
		}
		else if(req.body.encounterSelect == 'teledermatology') {
			res.render('./questionnaireForms/teledermatology_questionnaire', {user: req.session.username});
		}
		else if(req.body.encounterSelect == 'radiology') {
			res.render('./questionnaireForms/radiology_questionnaire', {user: req.session.username});
		}
		else if(req.body.encounterSelect == 'surgery_followup') {
			res.render('./questionnaireForms/surgery_followup_questionnaire', {user: req.session.username});
		}
		else if(req.body.encounterSelect == 'prenatal_screening') {
			res.render('./questionnaireForms/prenatal_screening_questionnaire', {user: req.session.username});
		}
		else if(req.body.encounterSelect == 'cervical_cancer') {
			res.render('./questionnaireForms/cervical_cancer_questionnaire', {user: req.session.username});
		}
		else if(req.body.encounterSelect == 'hiv_regimen_failure') {
			res.render('./questionnaireForms/hiv_regimen_questionnaire', {user: req.session.username});
		}
	});

});

/* GET admin page */
router.get('/addEncounter', function(req, res) {
	if(userCheck(req,res))
		res.render('./patientView/addEncounter', {user: req.session.username});
});

/* POST find patient page */
router.post('/addEncounter', function(req, res) {
	if(req.body.encounterSelect == "tb_assessment") {
		Encounter.init_tb_assessment(req, res, function(req, res, encounterList) {
			res.redirect('/profile');
		});
	}
});

/* POST for adding oral cancer form */
router.post('/oral_cancer_submit_form', function(req, res) {
	Encounter.add_oral_cancer(req, res, function(req, res, encounterList) {
		res.render('./patientView/encounter', encounterList)
	});	
});

/* POST for adding oral cancer form */
router.post('/tb_assessment_submit', function(req, res) {
	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	var encounterID = url.encounterID;
	var patientID = url.patientID;
	Encounter.add_tb_assessment(req, res, patientID, encounterID, function(req, res) {
		
		res.redirect('/profile');
	});	
});

router.post('/ophthalmolgy_submit_form', function(req, res) {
	Encounter.add_ophthalmolgy(req, res);
});

router.get('/addPatient', function(req, res) {

	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	var patientID = url.patientID;

	Doctor.getInfo(req, res, function(req, res, doctorInfo) {

		Patient.addRequest(req, res, patientID, doctorInfo.doctorID, function(req, res) {
			Patient.find_byID(req, res, patientID, function(req, res, patientInfo) {
				res.render('./doctorView/patientResults', {user: req.session.username, patientInfo: patientInfo, message: "Patient added successfully"});
			});
		});
	});
});

router.get('/historyRequests', function(req, res) {

	Patient.getRequests(req, res, function(req, res, requestList) {

		res.render('./patientView/historyRequests', {user: req.session.username, requestList: requestList});
	});
});

router.get('/acceptRequest', function(req, res) {

	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	var patientID = url.patientID;
	var doctorID = url.doctorID;

	Doctor.addPatient(req, res, patientID, doctorID, function(req, res) {
		Patient.addDoctor(req, res, patientID, doctorID, function(req, res) {
			Patient.removeDoctor(req, res, patientID, doctorID, function(req, res) {
				Patient.getRequests(req, res, function(req, res, requestList) {
					res.render('./patientView/historyRequests', {user: req.session.username, requestList: requestList});
				});
			});
		});
	});
});

router.get('/rejectRequest', function(req, res) {

	var url = querystring.parse(req.url.replace(/^.*\?/, ''));
	var patientID = url.patientID;
	var doctorID = url.doctorID;

	Patient.removeDoctor(req, res, patientID, doctorID, function(req, res) {
		Patient.getRequests(req, res, function(req, res, requestList) {
			res.render('./patientView/historyRequests', {user: req.session.username, requestList: requestList});
		});
	});
});

router.get('/newPrescription', function(req, res) {

	if(userCheck(req, res)) {
		res.render('./doctorView/newPrescription', {user: req.session.username});	
	}
});

router.post('/newPrescription', function(req, res) {

	Prescription.add(req, res, req.body.patientID, function(req, res) {
		res.redirect('/profile');	
	});
});

router.get('/encounterDetails', function(req, res) {
	
	if(userCheck(req, res)) {
		var url = querystring.parse(req.url.replace(/^.*\?/, ''));
		var encounterID = url.encounterID;
		var patientID = url.patientID;
		Encounter.get_tb_assessment(req, res, patientID, encounterID, function(req, res, encounterInfo) {
			res.render('./encounterDetails/tb_assessment', {user: req.session.username, encounterInfo: encounterInfo});
		});
	}
});

router.get('/manageDoctors', function(req, res) {

	if(userCheck(req, res)) {

		Nurse.find(req, res, function(req, res, nurseInfo) {
			var doctorList = [];
			for(var i = 0; i < nurseInfo.doctorList_ID.length; i++) {
				doctorList.push({doctorID: nurseInfo.doctorList_ID[i], doctorName: nurseInfo.doctorList_name[i]});
			}
			var recentEvent;
			if(req.session.recentEvent)
				recentEvent = req.session.recentEvent;
			else
				recentEvent = null;

			res.render('./nurseView/manageDoctors', {user: req.session.username, doctorList: doctorList, recentEvent: recentEvent});
		});
	}
});

router.post('/requestPatientList', function(req, res) {

	Doctor.addNurseRequest(req, res, req.body.doctorID, function(req, res) {
		req.session.recentEvent = "Request successfully sent!";
		res.redirect('/manageDoctors');
	});
});

router.get('/manageEncounters', function(req, res) {

	if(userCheck(req, res)) {
		Doctor.getRequests(req, res, function(req, res, requestList) {
			Doctor.getEncounters(req, res, function(req, res, encounterList) {
				res.render('./doctorView/manageEncounters', {user: req.session.username, requestList: requestList, encounterList: encounterList});
			});
		});
	}
});

router.get('/acceptNurse', function(req, res) {

	if(userCheck(req, res)) {
		var url = querystring.parse(req.url.replace(/^.*\?/, ''));
		var nurseID = url.nurseID;

		Doctor.addNurse(req, res, nurseID, function(req, res, objectID, doctorID, doctorName) {
			Nurse.addDoctor(req, res, nurseID, objectID, doctorID, doctorName, function(req, res) {
				res.redirect('/manageEncounters');
			});
		});
	}
});

router.get('/rejectNurse', function(req, res) {

	if(userCheck(req, res)) {
		var url = querystring.parse(req.url.replace(/^.*\?/, ''));
		var nurseID = url.nurseID;

		Doctor.removeNurse(req, res, nurseID, function(req, res) {
			res.redirect('/pendingRequests');
		});
	}
});

router.get('/removeDoctor', function(req, res) {

	if(userCheck(req,res)) {
		var url = querystring.parse(req.url.replace(/^.*\?/, ''));
		var doctorID = url.doctorID;
		Nurse.removeDoctor(req, res, doctorID, function(req, res) {
			Doctor.removeNurse(req, res, req.session.nurseID, function(req, res) {
				res.redirect('/manageDoctors');
			});
		});
	}
});

router.get('/managePrescriptions', function(req, res) {

	if(userCheck(req,res)) {
		Patient.find(req, res, function(req, res, patientList) {
			Prescription.getPrescriptions(req, res, patientList, function(req, res, prescriptionList) {
				res.render('./patientView/managePrescriptions', {user: req.session.username, prescriptionList: prescriptionList});
			});
		});
	}
});

router.post('/shareEncounters', function(req, res) {
	
	/* Check to see if more than one encounter was selected and pull out the object id's*/
	var encounters = req.body.encounters;
	var encounterList_ID = [];
	var encounterList_parentID = [];
	if(Object.prototype.toString.call(req.body.encounters) == '[object Array]') {
		for(var i = 0; i < encounters.length; i++) {
			var split = encounters[i].split('?');
			encounterList_ID.push(split[1]);
			encounterList_parentID.push(split[0]);
		}
	}
	else {	
		var split = encounters.split('?');
		encounterList_ID.push(split[1]);
		encounterList_parentID.push(split[0]);
	}

	var doctorID = req.body.doctorID;
	if(doctorID) {
		Encounter.addDoctor(req, res, encounterList_parentID, encounterList_ID, doctorID, function(req, res) {
			res.redirect('/manageEncounters');
		});
	}
});

module.exports = router;
